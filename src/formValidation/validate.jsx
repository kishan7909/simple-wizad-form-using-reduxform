const validate = values => {
  const errors = {};
  if (!values.firstname) {
    errors.firstname = "Required";
  }
  if (!values.lastname) {
    errors.lastname = "Required";
  }
  if (!values.email) {
    errors.email = "Required";
  }
  if (!values.sex) {
    errors.sex = "Required";
  }
  if (values.favcolor === "Select") {
    errors.favcolor = "Required";
  }
  return errors;
};

export default validate;
