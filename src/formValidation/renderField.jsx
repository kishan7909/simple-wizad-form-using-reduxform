import React from "react";
import { Label, FormGroup } from "reactstrap";

const renderField = ({
  input,
  name,
  label,
  data,

  type,
  meta: { touched, error }
}) => (
  <FormGroup>
    <Label>{label}</Label>
    {type === "input" || type === "email" || type === "checkbox" ? (
      <input
        {...input}
        type={type}
        name={name}
        placeholder={label}
        className="form-control"
      />
    ) : null}
    {type === "select" ? (
      <select {...input} name={name} className="form-control">
        {data.map((data, index) => (
          <option key={index} value={data}>
            {data}
          </option>
        ))}
      </select>
    ) : null}
    {type === "textarea" ? (
      <textarea
        {...input}
        name={name}
        placeholder={label}
        className="form-control"
      />
    ) : null}
    {touched && (error && <strong className="text-danger">{error}</strong>)}
  </FormGroup>
);

export default renderField;
