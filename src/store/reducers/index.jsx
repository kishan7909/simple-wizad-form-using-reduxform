import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";

console.log("rootReducer Call");
export const rootReducer = combineReducers({
  form: formReducer
});
