import React, { Component } from "react";
import { Col, Row, Card, CardBody, CardHeader } from "reactstrap";
import WizardForm from "../containers/WizardForm";

class Wizard extends Component {
  submit = values => {
    console.log(values);
    alert(JSON.stringify(values, "", 2));
  };
  render() {
    return (
      <Row>
        <Col md="6" className="offset-md-3">
          <Card>
            <CardHeader className="bg-info text-white">
              <h2>Wizard Form</h2>
            </CardHeader>
            <CardBody>
              <WizardForm onSubmit={this.submit} />
            </CardBody>
          </Card>
        </Col>
      </Row>
    );
  }
}

export default Wizard;
