import React from "react";
import renderField from "../formValidation/renderField";
import { Field, reduxForm } from "redux-form";
import { Button } from "reactstrap";
import validate from "../formValidation/validate";

let WizardPage3 = props => {
  const { handleSubmit, previousPage } = props;
  return (
    <form onSubmit={handleSubmit}>
      <Field
        component={renderField}
        name="favcolor"
        label="Favorite Color"
        type="select"
        data={["Select", "Red", "Blue", "Orange", "Yellow"]}
      />
      <Field
        component={renderField}
        name="employed"
        label="Employed"
        type="checkbox"
      />
      <Field component={renderField} name="note" label="Note" type="textarea" />

      <Button
        type="button"
        onClick={previousPage}
        className="m-2"
        color="btn btn-outline-info"
      >
        Previous
      </Button>
      <Button type="submit" color="btn btn-outline-info" className="m-2">
        Submit
      </Button>
    </form>
  );
};

WizardPage3 = reduxForm({
  form: "wizard",
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true,
  initialValues: { favcolor: "Select" },
  validate
})(WizardPage3);

export default WizardPage3;
