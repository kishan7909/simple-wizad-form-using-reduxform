import React from "react";
import renderField from "../formValidation/renderField";
import { Field, reduxForm } from "redux-form";
import { Button, Label } from "reactstrap";
import validate from "../formValidation/validate";
let WizardPage2 = props => {
  const { handleSubmit, previousPage } = props;
  return (
    <form onSubmit={handleSubmit}>
      <Field component={renderField} name="email" label="Email" type="email" />

      <div className="form-group">
        <Label>Sex</Label>
        <div className="form-inline">
          <div className="form-check m-1">
            <Field
              component="input"
              className="form-check-input"
              type="radio"
              name="sex"
              value="male"
              id="male"
            />
            <label className="form-check-label" htmlFor="male">
              Male
            </label>
          </div>
          <div className="form-check m-1">
            <Field
              component="input"
              className="form-check-input"
              type="radio"
              name="sex"
              value="female"
              id="female"
            />
            <label className="form-check-label" htmlFor="female">
              Female
            </label>
          </div>
        </div>
      </div>

      <Button
        type="button"
        onClick={previousPage}
        className="m-2"
        color="btn btn-outline-info"
      >
        Previous
      </Button>
      <Button type="submit" color="btn btn-outline-info" className="m-2">
        Next
      </Button>
    </form>
  );
};

WizardPage2 = reduxForm({
  form: "wizard",
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true,
  initialValues: { sex: "male" },
  validate
})(WizardPage2);

export default WizardPage2;
