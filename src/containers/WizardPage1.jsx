import React from "react";
import renderField from "../formValidation/renderField";
import { Field, reduxForm } from "redux-form";
import { Button } from "reactstrap";
import validate from "../formValidation/validate";

let WizardPage1 = props => {
  const { handleSubmit } = props;
  return (
    <form onSubmit={handleSubmit}>
      <Field
        component={renderField}
        name="firstname"
        label="Firstname"
        type="input"
      />
      <Field
        component={renderField}
        name="lastname"
        label="Lastname"
        type="input"
      />
      <Button type="submit" color="btn btn-outline-info">
        Next
      </Button>
    </form>
  );
};

WizardPage1 = reduxForm({
  form: "wizard",
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true,
  validate
})(WizardPage1);

export default WizardPage1;
