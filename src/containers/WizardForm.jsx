import React, { Component } from "react";
import WizardPage1 from "./WizardPage1";
import WizardPage2 from "./WizardPage2";
import WizardPage3 from "./WizardPage3";

export class WizardForm extends Component {
  state = {
    page: 1
  };
  nextPage = () => {
    this.setState({ page: this.state.page + 1 });
  };
  previousPage = () => {
    this.setState({ page: this.state.page - 1 });
  };
  render() {
    const { page } = this.state;
    const { onSubmit } = this.props;
    return (
      <div>
        {page === 1 && <WizardPage1 onSubmit={this.nextPage} />}
        {page === 2 && (
          <WizardPage2
            previousPage={this.previousPage}
            onSubmit={this.nextPage}
          />
        )}
        {page === 3 && (
          <WizardPage3 previousPage={this.previousPage} onSubmit={onSubmit} />
        )}
      </div>
    );
  }
}

export default WizardForm;
