import React, { Component } from "react";
import Wizard from "./components/Wizard";

class App extends Component {
  render() {
    return (
      <div className="container">
        <Wizard />
      </div>
    );
  }
}

export default App;
